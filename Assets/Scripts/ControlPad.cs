﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlPad : MonoBehaviour 
{
	public 	float 	    controlSpeed = 3f;

	private Vector3 	padPosition;
	private Vector3 	padStartPos;
	private float 		padPositionBottomOffset = 3f;
	private Transform 	padTransform;
	private float 		camHeight;
	private float 		camWidth;
	private Transform	padUI;

	void Start()
	{
		padPosition  = transform.position;
		padTransform = transform.GetChild(0);
		camHeight 	 = Camera.main.orthographicSize * 2;
		camWidth  	 = Camera.main.aspect * camHeight;
		padPosition  = new Vector3( 0, -( camHeight / 2 ) + padPositionBottomOffset, 0 );
		padStartPos	 = padPosition;
		padUI		 = transform.GetChild(1).transform.GetChild(0);
	}
	
	void Update () 
	{
		ControlPadMovement();
		UpdatePadUI();
	}

	private void ControlPadMovement()
	{
		if ( SceneManager.livesAmount != 0 )
		{
			if ( !SceneManager.isBallInPlay )
			{
				transform.position = padStartPos;
			}
			float xPosition 	= transform.position.x + ( Input.GetAxis( "Horizontal" ) * controlSpeed );
			float xPositionMax	= ( camWidth / 2 ) - padTransform.localScale.x / 2;
			padPosition 		= new Vector3 ( Mathf.Clamp ( xPosition, -xPositionMax, xPositionMax ), padPosition.y, 0 );
			transform.position 	= padPosition;
		}
	}

	private void UpdatePadUI()
	{
		Image lifeImageOne 	 = padUI.GetChild(0).GetComponent<Image>();
		Image lifeImageTwo 	 = padUI.GetChild(1).GetComponent<Image>();
		Image lifeImageThree = padUI.GetChild(2).GetComponent<Image>();

		lifeImageOne.color 	 = LevelBall.currentBallColor;
		lifeImageTwo.color   = LevelBall.currentBallColor;
		lifeImageThree.color = LevelBall.currentBallColor;

		switch ( SceneManager.livesAmount )
		{
		case 0:
			lifeImageOne.enabled 	= false;
			lifeImageTwo.enabled 	= false;
			lifeImageThree.enabled 	= false;
			break;
		case 1:
			lifeImageOne.enabled 	= true;
			lifeImageTwo.enabled 	= false;
			lifeImageThree.enabled 	= false;
			break;
		case 2:
			lifeImageOne.enabled 	= true;
			lifeImageTwo.enabled 	= true;
			lifeImageThree.enabled 	= false;
			break;
		case 3:
			lifeImageOne.enabled 	= true;
			lifeImageTwo.enabled 	= true;
			lifeImageThree.enabled 	= true;
			break;
		}
	}
}
