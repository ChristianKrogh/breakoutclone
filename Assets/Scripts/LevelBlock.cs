﻿using UnityEngine;
using System.Collections;

public class LevelBlock : MonoBehaviour 
{
	public int 			   blockHealth { set; get; }
	public BlockType 	   blockType = BlockType.normal;

	private SpriteRenderer blockRenderer;
	private Color 		   currentBlockColor;

	public enum BlockType
	{
		normal,
		powerup
	}

	void Start()
	{
		blockRenderer = GetComponent<SpriteRenderer>();
		gameObject.TriggerAnimation("scaleUp");
	}

	void Update()
	{
		switch( blockType )
		{
		case BlockType.normal:
			UpdateColor();
			break;
		case BlockType.powerup:
			blockRenderer.sortingOrder = 3;
			gameObject.SetBoolAnimation("normalPowerup", true);
			currentBlockColor = Utilities.hexToColor("577590");
			break;
		}

		blockRenderer.color = currentBlockColor;
	}

	void OnCollisionEnter2D( Collision2D other ) 
	{
		if ( other.gameObject.tag.Equals( Tags.ball ) )
		{
			SceneManager.currentScore++;

			LevelBall.currentBallColor = currentBlockColor;

			if ( blockType.Equals( BlockType.powerup ) )
			{
				CreateExplosion();
			}

			if ( blockHealth == 1 )
			{
				gameObject.PlaySound( Utilities.LoadAudioClip("sound_blockDestroy"), 0.1f );
				gameObject.TriggerAnimation("destroy");
			}
			else
			{
				gameObject.TriggerAnimation("hit");
				blockHealth--;
			}
		}
	}

	private void UpdateColor()
	{
		switch( blockHealth )
		{
		case 1: currentBlockColor = Utilities.hexToColor("DE5F51FF");
			break;
		case 2: currentBlockColor = Utilities.hexToColor("EE6352"); 
			break;
		case 3: currentBlockColor = Utilities.hexToColor("F4C769FF"); 
			break;
		case 4: currentBlockColor = Utilities.hexToColor("FFD166"); 
			break;
		case 5: currentBlockColor = Utilities.hexToColor("80B679FF"); 
			break;
		case 6: currentBlockColor = Utilities.hexToColor("82C17B"); 
			break;
		default:
			currentBlockColor = Color.white;
			break;
		}
	}

	// Used by animationEvent
	public void DestroyBlock() 
	{
		if ( transform.parent != null )
		{
			Destroy( transform.parent.gameObject );
		}
		else
		{
			Destroy( gameObject );
		}
	}

	private void CreateExplosion( float radius = 5.0f, float power = 0.5f )
	{
		Vector2	     explosionPos 	  = transform.position;
		Collider2D[] collidersHit 	  = Physics2D.OverlapCircleAll( explosionPos, radius, 1 << 8 ); // Layer 8 = LevelBlocks
		int 		 tempGainedScore  = 0;

		gameObject.PlaySound( Utilities.LoadAudioClip("sound_explosion_01"), 0.4f );

		foreach ( Collider2D hit in collidersHit ) 
		{
			Rigidbody2D hitRigidbody = hit.GetComponent<Rigidbody2D>();
			Vector2 	hitPosition  = new Vector2( hitRigidbody.transform.position.x, hitRigidbody.transform.position.y );
			LevelBlock 	hitLevelBall = hit.GetComponent<LevelBlock>();
			tempGainedScore	+= hitLevelBall.blockHealth;

			if ( hitRigidbody != null )
			{
				hitRigidbody.isKinematic 	  = false;
				hitRigidbody.transform.parent = null;
				hitRigidbody.gameObject.TriggerAnimation("destroy");
				Vector2 direction 			  = power * ( hitPosition - explosionPos );
				hitRigidbody.AddForce( direction, ForceMode2D.Impulse );
			}
		}

		SceneManager.currentScore += tempGainedScore;
	}
}






