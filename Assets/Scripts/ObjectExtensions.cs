﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ObjectExtensions 
{
	#region Animation
	public static void TriggerAnimation( this GameObject gameObject, string animName )
	{
		Animator animator = gameObject.GetComponent<Animator>();
		if ( !animator.GetCurrentAnimatorStateInfo(0).IsName( animName ) )
		{
			animator.SetTrigger( animName );
		}
	}
	public static void SetBoolAnimation( this GameObject gameObject, string animName, bool animationState )
	{
		Animator animator = gameObject.GetComponent<Animator>();
		if ( !animator.GetCurrentAnimatorStateInfo(0).IsName( animName ) )
		{
			animator.SetBool( animName, animationState );
		}
	}
	#endregion

	#region Sound
	// With delay
	public static IEnumerator PlaySound( this GameObject sourceObject, float delay, AudioClip sound, float volumeScale = 0.5f )
	{
		yield return new WaitForSeconds( delay );
		sourceObject.PlaySound( sound, false, volumeScale );
	}
	public static IEnumerator PlaySound( this GameObject sourceObject, float delay, AudioClip sound, bool loop, float volumeScale = 0.5f )
	{
		yield return new WaitForSeconds( delay );
		sourceObject.PlaySound( sound, loop, volumeScale );
	}
	// Without delay
	public static void PlaySound( this GameObject sourceObject, AudioClip sound, float volumeScale = 0.5f )
	{
		sourceObject.PlaySound( sound, false, volumeScale );
	}
	public static void PlaySound( this GameObject sourceObject, AudioClip sound, bool loop, float volumeScale = 0.5f )
	{
		if ( sound != null )
		{
			AudioSource audio = null;
			
			if ( sourceObject.GetComponent<AudioSource>() != null )
			{
				//Debug.LogWarning( "Utilities.cs: '" + sourceObject.name + "' is playing the following sound: '" + sound.name + "'" );
				audio = sourceObject.GetComponent<AudioSource>();
				if ( !loop )
				{
					audio.PlayOneShot( sound, volumeScale );
				}
				else
				{
					audio.volume = volumeScale;
					audio.loop = true;
					audio.clip = sound;
					audio.Play();
				}
			}
			else
			{
				//Debug.LogWarning( "Utilities.cs: Attaching an AudioSource to '" + sourceObject.name + "' because one is needed to play audioclip: '" + sound.name + "'" );
				sourceObject.AddComponent<AudioSource>();
				audio = sourceObject.GetComponent<AudioSource>();
				if ( !loop )
				{
					audio.PlayOneShot( sound, volumeScale );
				}
				else
				{
					audio.volume = volumeScale;
					audio.loop = true;
					audio.clip = sound;
					audio.Play();
				}
			}
		}
	}
	// With delay
	public static IEnumerator ShuffleSound( this GameObject sourceObject, float delay, List<AudioClip> soundList, List<AudioClip> usedSoundsList, float volumeScale = 0.5f )
	{
		yield return new WaitForSeconds( delay );
		sourceObject.ShuffleSound( soundList, usedSoundsList, volumeScale );
	}
	// Without delay
	public static void ShuffleSound( this GameObject sourceObject, List<AudioClip> soundList, List<AudioClip> usedSoundsList, float volumeScale = 0.5f )
	{
		if ( usedSoundsList.Count == 0 )
		{
			foreach ( AudioClip sound in soundList )
			{
				usedSoundsList.Add( sound );
			}
		}
		if ( soundList.Count != 0 )
		{
			AudioSource audio 			= null;
			int 		randomIndex 	= Random.Range (0, usedSoundsList.Count ); 		
			AudioClip 	randomAudioClip = usedSoundsList[randomIndex];
			usedSoundsList.RemoveAt( randomIndex );	
			
			if ( sourceObject.GetComponent<AudioSource>() != null )
			{
				//Debug.LogWarning( "Utilities.cs: '" + sourceObject.name + "' is playing the following sound: '" + randomAudioClip.name + "'" );
				audio = sourceObject.GetComponent<AudioSource>();
				audio.PlayOneShot( randomAudioClip, volumeScale );
			}
			else
			{
				//Debug.LogWarning( "Utilities.cs: Attaching an AudioSource to '" + sourceObject.name + "' because one is needed to play audioclip: '" + randomAudioClip.name + "'" );
				sourceObject.AddComponent<AudioSource>();
				audio = sourceObject.GetComponent<AudioSource>();
				audio.PlayOneShot( randomAudioClip, volumeScale );
			}
		}
	}
	#endregion
}
