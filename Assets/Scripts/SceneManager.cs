﻿using UnityEngine;
using System.Collections;

public class SceneManager : MonoBehaviour 
{
	public static int  currentScore 	= 0;
	public static int  livesAmount  	= 3;
	public static int  blockCount   	= 0;
	public static bool levelComplete	= false;
	public static bool reloadLevel  	= false;
	public static bool isBallInPlay 	= false;
	public static bool isLevelGenerated = false;

	public  int 	   menuLevelIndex;
	private float 	   loadMenuDelay   = 2f;

	void Start()
	{
		SceneManager.isBallInPlay = false;
		SceneManager.currentScore = 0;
		SceneManager.livesAmount  = 3;
	}

	void Update()
	{
		if ( blockCount == 0 && livesAmount > 0 && isBallInPlay == true )
		{
			levelComplete = true;
		}
		if ( livesAmount <= 0 )
		{
			StartCoroutine(LoadMenuScene());
		}
	}

	private IEnumerator LoadMenuScene()
	{
		yield return new WaitForSeconds(loadMenuDelay);
		Application.LoadLevel( menuLevelIndex );
	}
}
