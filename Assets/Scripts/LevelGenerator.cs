﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelGenerator : MonoBehaviour 
{
	public  GameObject 	 levelBlock;
	public  string 		 csvLevelName;
	
	private List<string> csvLevelData 	= new List<string>();
	private Vector3 	 topLeftCorner;
	private Vector3 	 blockSize  	= new Vector3( 0.0f, 1f, 0);
	private float 		 topOffset		= 0.5f;
	private float 		 reloadDelay    = 1f;

	void Start () 
	{
		GenerateLevel( csvLevelName );
	}

	void Update()
	{
		SceneManager.blockCount = transform.childCount;

		if ( SceneManager.reloadLevel )
		{
			SceneManager.reloadLevel = false;
			StartCoroutine( ReloadLevel() );
		}
	}

	private void GenerateLevel( string csvFileName = "" )
	{
		float camHalfHeight;
		float camHalfWidth;
		float blockXPos = 0;
		int   rowNumber = 0;

		csvLevelData 	= CSVReader.ReadData( csvFileName );
		camHalfHeight 	= Camera.main.orthographicSize;
		camHalfWidth  	= Camera.main.aspect * camHalfHeight; 
		topLeftCorner 	= -( new Vector3( camHalfWidth, -camHalfHeight, 0 ) + Camera.main.transform.position );

		if ( csvLevelData.Count != 0 )
		{
			// Set the width of the blocks 
			blockSize.x = -( ( topLeftCorner.x / CSVReader.GetWidestRow(csvLevelData) ) * 2 ); 

			for ( int i = 0; i < csvLevelData.Count; i++ )
			{
				string cell 	 = csvLevelData[i];
				int    cellValue = 0;			
				blockXPos 	 	+= blockSize.x;
			
				// Check if cell value == number
				if ( int.TryParse( cell, out cellValue ) )											
				{
					CreateBlock( blockXPos, rowNumber, LevelBlock.BlockType.normal, cellValue );
				}
				// Check if cell value == powerup
				if ( cell.Equals("x") )																
				{
					CreateBlock( blockXPos, rowNumber, LevelBlock.BlockType.powerup );
				}
				// Check if cell value == row break 
				if ( cell.Equals("\n") ) 														
				{
					blockXPos = 0;
					rowNumber++;
				}
			}
		}
		transform.position = topLeftCorner;
		SceneManager.isLevelGenerated   = true;
	}

	private void CreateBlock( float posX, float posY, LevelBlock.BlockType blockType, int blockHealth = 1 )  
	{
		GameObject newBlock 		  = Instantiate( levelBlock, new Vector3( posX - ( blockSize.x / 2 ), ( -posY - ( blockSize.y / 2 ) + topOffset ) ), Quaternion.identity ) as GameObject;
		newBlock.transform.parent 	  = transform;
		newBlock.transform.localScale = blockSize;
		newBlock.transform.GetChild(0).GetComponent<LevelBlock>().blockType   = blockType;
		newBlock.transform.GetChild(0).GetComponent<LevelBlock>().blockHealth = blockHealth;
	}

	private IEnumerator ReloadLevel()
	{
		if ( transform.childCount != 0 )
		{
			SceneManager.isLevelGenerated = false;
			
			foreach ( Transform child in transform )
			{
				if ( child.childCount != 0 )
				{
					if ( child.transform.GetChild(0).GetComponent<Animator>() )
					{
						child.transform.GetChild(0).gameObject.TriggerAnimation("destroy");
					}
				}
				else
				{
					Destroy( child.gameObject );
				}
			}

			if ( SceneManager.livesAmount != 0 )
			{
				yield return new WaitForSeconds( reloadDelay );
				
				transform.position = Vector3.zero;
				
				GenerateLevel( csvLevelName );
			}
		}
	}
}
