﻿using UnityEngine;
using System.Collections;

public static class Tags 
{
	public const string controlPad  = "ControlPad";
	public const string deadzone	= "Deadzone";
	public const string ball		= "Ball";
	public const string wall		= "Wall";
	public const string block		= "LevelBlock";
}
