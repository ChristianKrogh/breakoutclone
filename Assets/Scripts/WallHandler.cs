﻿using UnityEngine;
using System.Collections;

public class WallHandler : MonoBehaviour 
{
	public bool menuSceneRdy = false;

	void Start () 
	{
		SetupWalls();
	}
	
	private void SetupWalls()
	{
		Transform wallTop   = transform.GetChild(0);
		Transform wallLeft  = transform.GetChild(1);
		Transform wallRight = transform.GetChild(2);
		Transform deadzone  = transform.GetChild(3);
		float camHeight 	= Camera.main.orthographicSize * 2;
		float camWidth  	= Camera.main.aspect * camHeight;
		float bottomOffset  = 2.5f;

		wallTop.transform.position   = new Vector3( 0, (camHeight / 2 ) + 0.5f,   0    );
		wallLeft.transform.position  = new Vector3( -( (camWidth / 2 )  + 0.5f ), 0, 0 );
		wallRight.transform.position = new Vector3(  ( (camWidth / 2 )  + 0.5f ), 0, 0 );

		if ( menuSceneRdy )
		{
			bottomOffset = 0.5f;
		}

		deadzone.transform.position  = new Vector3( 0, -(camHeight / 2) - bottomOffset, 0 );
	}
	
}
