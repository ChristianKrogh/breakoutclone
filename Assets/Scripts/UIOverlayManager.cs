﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIOverlayManager : MonoBehaviour 
{
	private Transform textContainerMid;
	
	void Start () 
	{
		textContainerMid = transform.GetChild(0);
	}

	void Update () 
	{
		if ( !SceneManager.isBallInPlay && SceneManager.livesAmount != 0 && SceneManager.isLevelGenerated )
		{
			UpdateTextNotification( true, "Press the 'Space-bar' to start..." );
		}
		else
		{
			UpdateTextNotification();
		}
	}

	private void UpdateTextNotification( bool active = false, string text = "" )
	{
		Text notificationText = textContainerMid.transform.GetChild(0).GetComponent<Text>();
		notificationText.text = text;
		notificationText.gameObject.SetActive( active );
	}
}
