using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelBall : MonoBehaviour 
{
	public  float	 		speed = 30;	
	public  float 			speedIncreaseFactor = 0.5f;
	public  float 			initialSpeed;
	public  bool 			autoPlay = false;

	private SpriteRenderer  ballRenderer;
	private Rigidbody2D 	ballRigidBody;
	private Vector2	 		startDirection;
	private Vector2[]		startPositions 	= { new Vector2(-10, 0), new Vector2(10, 0) };
	private List<AudioClip> hitSounds 	  	= new List<AudioClip>();
	private List<AudioClip> usedHitSounds 	= new List<AudioClip>();

	public static Color	currentBallColor { set; get; } 

	void Start()
	{
		initialSpeed  	   = speed;
		ballRenderer	   = GetComponent<SpriteRenderer>();
		ballRigidBody 	   = GetComponent<Rigidbody2D>();
		currentBallColor   = Utilities.hexToColor("FFD166"); 
		FindStartDirection();
		SetupAudio();
	}

	void Update () 
	{
		ballRenderer.color = currentBallColor;

		if ( SceneManager.levelComplete && ballRigidBody.isKinematic == false )
		{
			ballRigidBody.isKinematic = true;
			ballRenderer.enabled 	  = false;
			gameObject.PlaySound( Utilities.LoadAudioClip("sound_theFirstWin"), 0.4f );
		}

		if ( SceneManager.isLevelGenerated )
		{
			if ( Input.GetKeyDown( KeyCode.Space ) && SceneManager.isBallInPlay == false || autoPlay && SceneManager.isBallInPlay == false )
			{
				ballRenderer.enabled 	  = true;
				SceneManager.isBallInPlay = true;
				ballRigidBody.isKinematic = false;
				ballRigidBody.velocity    = startDirection.normalized * speed;
			}
		}
		if ( SceneManager.livesAmount == 0 || !SceneManager.isBallInPlay )
		{
			ballRenderer.enabled = false;
		}
	}

	private void FindStartDirection()
	{
		int randomStartIndex = Random.Range(0, startPositions.Length);
		transform.position   = startPositions[ randomStartIndex ];
		
		if ( randomStartIndex.Equals(0) )
		{
			startDirection = new Vector2( 1, -1 );
		}
		else
		{
			startDirection = new Vector2( -1, -1 );
		}
	}
	
	void OnCollisionEnter2D( Collision2D other ) 
	{
		if ( other.gameObject.tag.Equals( Tags.controlPad ) )
		{
			float 	x 	= HitFactor( transform.position, other.transform.position, other.collider.bounds.size.x );
			Vector2 dir = new Vector2( x, 1 ).normalized;
			ballRigidBody.velocity = dir * speed;
		}
		if ( other.gameObject.tag.Equals( Tags.wall ) || other.gameObject.tag.Equals( Tags.controlPad ) || other.gameObject.tag.Equals( Tags.block ) )
		{		
			gameObject.TriggerAnimation("hit");
			gameObject.ShuffleSound( hitSounds, usedHitSounds, 0.25f );
		}
		if ( other.gameObject.tag.Equals( Tags.block ) )
		{
			speed += speedIncreaseFactor;
		}
		if ( other.gameObject.tag.Equals( Tags.deadzone ) )
		{
			SceneManager.isBallInPlay = false;
			ballRigidBody.isKinematic = true;
			FindStartDirection();

			if ( SceneManager.livesAmount != 0 )
			{
				speed = initialSpeed;
				SceneManager.currentScore = 0;
				SceneManager.livesAmount--;
				SceneManager.reloadLevel = true;
			}
		}
	}

	private float HitFactor( Vector2 ballPos, Vector2 controlPadPos, float controlPadWidth )
	{
		// 					 (Middle)
		// (Left side =) -1     0     1 (= Right side)
		// 				 =============== <- ControlPad
		return ( ballPos.x - controlPadPos.x ) / controlPadWidth;
	}

	private void SetupAudio()
	{
		hitSounds = Utilities.CreateAudioList
		(
			Utilities.LoadAudioClip("sound_ballHit_01"),
			Utilities.LoadAudioClip("sound_ballHit_02"),
			Utilities.LoadAudioClip("sound_ballHit_03"),
			Utilities.LoadAudioClip("sound_ballHit_04"),
			Utilities.LoadAudioClip("sound_ballHit_05")
		);
	}
}
