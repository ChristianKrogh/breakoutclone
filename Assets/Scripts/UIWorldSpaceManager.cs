﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIWorldSpaceManager : MonoBehaviour 
{
	private Text scoreText;
	private Text speedText;
	private Image rewardImage;
	private LevelBall ball;
	
	void Start()
	{
		scoreText	= transform.GetChild(0).GetComponent<Text>();
		speedText 	= transform.GetChild(1).GetComponent<Text>();
		rewardImage = transform.GetChild(2).GetComponent<Image>();
		ball	  	= GameObject.FindGameObjectWithTag( Tags.ball ).GetComponent<LevelBall>();
	}
	
	void Update () 
	{
		if ( SceneManager.levelComplete )
		{
			scoreText.enabled 	= false;
			speedText.enabled 	= false;
			rewardImage.enabled = true;
		}
		else
		{
			scoreText.text = SceneManager.currentScore.ToString();	
			speedText.text = ball.speed.ToString("f0") + "\nSPEED";
			rewardImage.enabled = false;
		}
	}
}
