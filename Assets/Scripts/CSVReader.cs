﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class CSVReader 
{
	public static List<string> ReadData( string csvFileName = "", char rowSeperater = '\n', char cellSeperator = ';' )
	{
		List<string> csvData = new List<string>();
		TextAsset 	 csvFile = Resources.Load( csvFileName ) as TextAsset;
		if ( csvFile )
		{
			string[] rows = csvFile.text.Split( rowSeperater ); 
			
			foreach ( string row in rows )
			{
				string[] cells = row.Split( cellSeperator );
				
				foreach( string cell in cells )
				{
					if ( !cell.Equals("") )
					{
						csvData.Add( cell );
					}
				}
				csvData.Add( "\n" );
			}
		}
		else
		{
			Debug.LogWarning( string.Format("CSVReader: The .csv file '{0}' could not be found in a resources-folder.", csvFileName ) );
		}
		return csvData;
	}

	// Return the widest row to help define the width of the level
	public static int GetWidestRow( List<string> csvLevelData )
	{
		int rowWidth 	= 0;
		int cellsInRow 	= 0;
		
		for ( int i = 0; i < csvLevelData.Count; i++ )
		{
			if ( csvLevelData[i].Equals("\n") )
			{
				if ( rowWidth <= cellsInRow )
				{
					rowWidth = cellsInRow;
				}
				cellsInRow = 0;
			}
			else
			{
				cellsInRow++;
			}
		}
		return rowWidth;
	}
}
