﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class LoadScene : MonoBehaviour, IPointerUpHandler  
{
	public int sceneToLoadIndex;

	public void OnPointerUp (PointerEventData data) 
	{
		Application.LoadLevel( sceneToLoadIndex );
	}
}
